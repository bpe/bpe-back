package fr.utc.bpe_back.repositories;

import fr.utc.bpe_back.models.AppSettings;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AppSettingsRepository extends CrudRepository<AppSettings, String> {

}