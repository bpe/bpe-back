package fr.utc.bpe_back.repositories.projects;

import fr.utc.bpe_back.models.projects.Project;
import fr.utc.bpe_back.models.projects.ProjectAttachment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface ProjectAttachmentRepository extends CrudRepository<ProjectAttachment, UUID> {
    @Query("SELECT pa FROM ProjectAttachment pa WHERE pa.project=:project")
    Iterable<ProjectAttachment> getAttachmentsByProject(@Param("project") Project project);

    @Query("SELECT pa FROM ProjectAttachment pa WHERE pa.project=:project AND pa.type<>fr.utc.bpe_back.models.projects.ProjectAttachmentType.BUDGET")
    Iterable<ProjectAttachment> getAttachmentsExceptBudgetByProject(@Param("project") Project project);

    void deleteByProject(Project project);
}