package fr.utc.bpe_back.repositories.projects;

import fr.utc.bpe_back.models.projects.Project;
import fr.utc.bpe_back.models.projects.ProjectVote;
import fr.utc.bpe_back.models.sessions.Session;
import fr.utc.bpe_back.models.users.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ProjectVoteRepository extends CrudRepository<ProjectVote, Long> {
    @Query("SELECT pv FROM ProjectVote pv WHERE pv.project=:project")
    Iterable<ProjectVote> getVotesByProject(@Param("project") Project project);

    @Query("SELECT pv FROM ProjectVote pv WHERE pv.project.session=:session AND pv.user=:user")
    Iterable<ProjectVote> getVotesBySessionAndUser(@Param("session") Session session, @Param("user") User user);

    @Query("SELECT COUNT(pv) FROM ProjectVote pv WHERE pv.project=:project")
    Integer getVoteCountByProject(@Param("project") Project project);

    @Query("SELECT Count(pv), pv FROM ProjectVote pv WHERE pv.project.session=:session GROUP BY pv.project")
    Iterable<Object[]> getVoteCountsBySession(@Param("session") Long session);

    void deleteByProject(Project project);

    @Modifying
    @Query("UPDATE ProjectVote pv SET pv.user=null WHERE pv.project in (SELECT p FROM Project p WHERE p.session=:session)")
    void anonymizeVotesForSession(Session session);

    @Query("SELECT pv FROM ProjectVote pv WHERE pv.project=:project AND pv.user=:user")
    Iterable<ProjectVote> getVoteByProjectAndUser(Project project, User user);
}