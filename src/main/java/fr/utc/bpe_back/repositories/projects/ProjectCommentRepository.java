package fr.utc.bpe_back.repositories.projects;

import fr.utc.bpe_back.models.projects.Project;
import fr.utc.bpe_back.models.projects.ProjectComment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface ProjectCommentRepository extends CrudRepository<ProjectComment, Long> {
    @Query("SELECT pc FROM ProjectComment pc WHERE pc.project=:project ORDER BY pc.createdAt ASC")
    Iterable<ProjectComment> getCommentsByProject(@Param("project") Project project);

    @Query("SELECT pc FROM ProjectComment pc WHERE pc.project=:project AND pc.published ORDER BY pc.createdAt ASC")
    Iterable<ProjectComment> getPublishedCommentsByProject(@Param("project") Project project);
    void deleteByProject(Project project);
}