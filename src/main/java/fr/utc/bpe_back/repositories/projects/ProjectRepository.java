package fr.utc.bpe_back.repositories.projects;

import fr.utc.bpe_back.models.DTO.ProjectSummaryDTO;
import fr.utc.bpe_back.models.projects.Project;
import fr.utc.bpe_back.models.projects.ProjectStatus;
import fr.utc.bpe_back.models.sessions.Session;
import fr.utc.bpe_back.models.users.User;
import jakarta.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
@Transactional
public interface ProjectRepository extends CrudRepository<Project, Long> {
    @Query("SELECT p FROM Project p WHERE p.session=:session AND NOT p.deleted")
    Iterable<Project> getProjectsBySession(@Param("session") Session s);

    @Query("SELECT p FROM Project p WHERE p.session=:session AND NOT p.deleted AND p.status=:status")
    Iterable<Project> getProjectsBySessionAndStatus(@Param("session") Session s, @Param("status") ProjectStatus status);


    @Query("SELECT p FROM Project p WHERE p.owner=:user AND NOT p.deleted")
    Iterable<Project> getProjectsByUser(@Param("user") User user);

    //    @Query("SELECT p FROM Project p WHERE p.session=:session AND NOT p.deleted")
    @Query("SELECT new fr.utc.bpe_back.models.projects.Project(p, count(pv)) FROM Project p LEFT JOIN ProjectVote pv ON pv.project=p WHERE p.session=:session AND NOT p.deleted GROUP BY p.id")
    Iterable<ProjectSummaryDTO> getProjectsSummaryBySession(@Param("session") Session s);

    //    @Query("SELECT p FROM Project p WHERE p.session=:session AND NOT p.deleted AND p.status=:status")
    @Query("SELECT new fr.utc.bpe_back.models.projects.Project(p, count(pv)) FROM Project p LEFT JOIN ProjectVote pv ON pv.project=p WHERE p.session=:session AND p.status=:status AND NOT p.deleted GROUP BY p.id")
    Iterable<ProjectSummaryDTO> getProjectsSummaryBySessionAndStatus(@Param("session") Session s, @Param("status") ProjectStatus status);

    @Query("SELECT p FROM Project p WHERE p.id=:id AND NOT p.deleted")
    @NotNull
    Optional<Project> findById(@Param("id") @NotNull Long id);

    @Query("SELECT p FROM Project p WHERE NOT p.deleted")
    @NotNull
    Iterable<Project> findAll();

    @Query("SELECT new fr.utc.bpe_back.models.projects.Project(p, count(pv)) FROM Project p LEFT JOIN ProjectVote pv ON pv.project=p WHERE p.session=:session AND p.owner=:user AND NOT p.deleted GROUP BY p.id")
    Iterable<ProjectSummaryDTO> getProjectsSummaryBySessionAndUser(@Param("session") Session session, @Param("user") User user);
}