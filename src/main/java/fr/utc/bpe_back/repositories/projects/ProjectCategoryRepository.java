package fr.utc.bpe_back.repositories.projects;

import fr.utc.bpe_back.models.projects.ProjectCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface ProjectCategoryRepository extends CrudRepository<ProjectCategory, Long> {

    @Query("SELECT pc FROM ProjectCategory pc WHERE pc.name=:name")
    Optional<ProjectCategory> findByName(@Param("name") String name);
}