package fr.utc.bpe_back.repositories;

import fr.utc.bpe_back.models.Token;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TokenRepository extends CrudRepository<Token, String> {
	@Query("SELECT t FROM Token t WHERE t.expiration < ?1")
	List<Token> findExpiredTokens(long expiration);

	@Query("DELETE FROM Token t WHERE t.expiration < ?1")
	@Transactional
	@Modifying
	void deleteExpiredTokens(long expiration);

	@Query("DELETE FROM Token t WHERE t.user = ?1")
	@Transactional
	@Modifying
	void deleteUserTokens(int userId);
}
