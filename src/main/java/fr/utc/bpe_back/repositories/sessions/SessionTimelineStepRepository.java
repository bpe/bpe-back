package fr.utc.bpe_back.repositories.sessions;

import fr.utc.bpe_back.models.sessions.Session;
import fr.utc.bpe_back.models.sessions.SessionTimelineStep;
import fr.utc.bpe_back.models.sessions.TimelineItem;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;


@Repository
public interface SessionTimelineStepRepository extends CrudRepository<SessionTimelineStep, Long> {

    @Query("SELECT sts FROM SessionTimelineStep sts WHERE sts.session=:session ORDER BY sts.start ASC")
    Set<SessionTimelineStep> getTimelineStepsForSession(@Param("session") Session session);

    @Query("DELETE FROM SessionTimelineStep sts WHERE sts.session = :session AND sts.timelineItem = :timelineItem")
    @Modifying
    void deleteBySessionAndName(@Param("session") Session session, @Param("timelineItem") TimelineItem timelineItem);

    @Query("SELECT sts FROM SessionTimelineStep sts WHERE sts.session = :session AND sts.timelineItem = :timelineItem")
    Optional<SessionTimelineStep> getTimelineStepByItemForSession(
            @Param("session") Session session,
            @Param("timelineItem") TimelineItem timelineItem
    );

    @Query("SELECT sts FROM SessionTimelineStep sts WHERE sts.session = :session AND sts.start <= CURRENT_DATE AND sts.end >= CURRENT_DATE ORDER BY sts.start ASC")
    Set<SessionTimelineStep> getActiveSessionTimelineStep(@Param("session") Session session);

    @Query("SELECT sts FROM SessionTimelineStep sts WHERE sts.session=:session AND sts.timelineItem.id=:name")
    Optional<SessionTimelineStep> getTimelineStepByNameForSession(@Param("session") Session s, @Param("name") String name);
}