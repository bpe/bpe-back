package fr.utc.bpe_back.repositories.sessions;

import fr.utc.bpe_back.models.sessions.TimelineItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TimelineItemRepository extends CrudRepository<TimelineItem, String> {
}