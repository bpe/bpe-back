package fr.utc.bpe_back.repositories.sessions;

import fr.utc.bpe_back.models.sessions.Session;
import jakarta.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
@Transactional()
public interface SessionRepository extends CrudRepository<Session, Long> {

    @Query("SELECT s FROM Session  s where s.slug = :slug")
    Session getSessionBySlug(@Param("slug") String slug);

    @Query("SELECT s FROM Session s ORDER BY s.createdAt DESC")
    @NotNull
    Iterable<Session> findAll();
    @Query("SELECT s FROM Session s WHERE s.deleted = false ORDER BY s.createdAt DESC")
    Iterable<Session> findAllByDeletedIsFalse();
}