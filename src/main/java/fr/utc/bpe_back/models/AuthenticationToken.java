package fr.utc.bpe_back.models;

import fr.utc.bpe_back.models.users.User;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Date;

public class AuthenticationToken extends UsernamePasswordAuthenticationToken {
	private final Date expiration;

	public AuthenticationToken(User user, Collection<? extends GrantedAuthority> authorities, Date expiration) {
		super(user, "DISABLED", authorities);
		this.expiration = expiration;
	}

	public Date getExpiration() {
		return expiration;
	}
}
