package fr.utc.bpe_back.models.projects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import fr.utc.bpe_back.models.sessions.Session;
import fr.utc.bpe_back.models.users.User;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "projects")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String slug;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "session_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Session session;

    @Column(nullable = false)
    private String title;

    @Lob
    @Column(nullable = false, columnDefinition = "TEXT")
    private String description;

    @Lob
    @Column(columnDefinition = "TEXT")
    private String details;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.SET_NULL)
    private ProjectCategory category;

    @Column(nullable = false)
    private ProjectStatus status = ProjectStatus.PENDING_VALIDATION;

    @Column(nullable = false)
    private Boolean realised = false;

    @Column(nullable = false)
    private Float budget;

    private Float allocatedBudget;

    @ManyToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name="user_id")
    @OnDelete(action = OnDeleteAction.SET_NULL)
    private User owner;

    private String phone;

    private String workgroup;

    @Transient
    private Integer votantsCount = 0;

    @OneToMany(mappedBy = "project")
    @JsonManagedReference
    private Set<ProjectComment> comments;

    @OneToMany(mappedBy = "project")
    @JsonManagedReference
    private Set<ProjectAttachment> attachments;

    @OneToMany(mappedBy = "project")
    @JsonManagedReference
    @JsonIgnore
    private Set<ProjectVote> votes;

    private Boolean deleted = false;

    @CreatedDate
    @JsonIgnore
    private LocalDateTime createdAt;

    @LastModifiedDate
    @JsonIgnore
    private LocalDateTime updatedAt;

    @LastModifiedBy
    @JsonIgnore
    private String modifiedBy;

    // For ProjectRepository.getProjectsSummaryBySession
    public Project(Project p, Long votantsCount) {
        this.id = p.id;
        this.slug = p.slug;
        this.session = p.session;
        this.title = p.title;
        this.description = p.description;
        this.details = p.details;
        this.category = p.category;
        this.status = p.status;
        this.realised = p.realised;
        this.budget = p.budget;
        this.allocatedBudget = p.allocatedBudget;
        this.owner = p.owner;
        this.phone = p.phone;
        this.workgroup = p.workgroup;
        this.votantsCount = votantsCount.intValue();
        this.comments = p.comments;
        this.attachments = p.attachments;
        this.deleted = p.deleted;
        this.createdAt = p.createdAt;
        this.updatedAt = p.updatedAt;
        this.modifiedBy = p.modifiedBy;
    }

    public Project() {

    }
}
