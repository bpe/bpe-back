package fr.utc.bpe_back.models.projects;

public enum ProjectStatus {
    ACCEPTED,
    NOT_ACCEPTED,
    ACTION_NEEDED,
    PENDING_VALIDATION,
    CANCELED
}
