package fr.utc.bpe_back.models.projects;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "project_categories")
public class ProjectCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
}
