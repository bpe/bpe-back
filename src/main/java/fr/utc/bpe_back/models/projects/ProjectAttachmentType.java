package fr.utc.bpe_back.models.projects;

public enum ProjectAttachmentType {
    IMAGE,
    HEADER_IMAGE,
    DESCRIPTION,
    OPTIONAL,
    BUDGET,
    ACHIEVEMENT
}
