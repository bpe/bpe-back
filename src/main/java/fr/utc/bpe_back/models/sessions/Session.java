package fr.utc.bpe_back.models.sessions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "sessions")
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(unique = true, name = "slug", nullable = false)
    private String slug;

    private Boolean deleted = false;

    @OneToMany(mappedBy = "session")
    @Transient
    private Set<SessionTimelineStep> timelineSteps = new HashSet<>();

    @CreatedDate
    @JsonIgnore
    private LocalDateTime createdAt;

    @LastModifiedDate
    @JsonIgnore
    private LocalDateTime updatedAt;

    @LastModifiedBy
    @JsonIgnore
    private String modifiedBy;

    public Session() {
    }

    public void setSlug(String slug) throws IllegalArgumentException {
        if (slug==null) throw new IllegalArgumentException("Slug cannot be null");
        // MAtch regex for slug
        if(!slug.matches("^[a-z0-9]+(?:-[a-z0-9]+)*$")) {
            throw new IllegalArgumentException("Slug must contain only lowercase letters, numbers and dashes");
        }
        this.slug = slug.toLowerCase();
    }

    public Date getEndDate() {
        if (timelineSteps.isEmpty()) return null;
        Date endDate = null;
        for (SessionTimelineStep step : timelineSteps) {
            if (step.getEnd() != null && (endDate == null || step.getEnd().after(endDate))) {
                endDate = step.getEnd();
            }
        }
        return endDate;
    }
}
