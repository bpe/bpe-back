package fr.utc.bpe_back.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.utc.bpe_back.models.users.Role;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.LocalDateTime;

@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "app_settings")

public class AppSettings {
    @Id
    private String id;

    private String description;

    private String value;

    @Enumerated(EnumType.STRING)
    private Role role;

    @CreatedDate
    @JsonIgnore
    private LocalDateTime createdAt;

    @LastModifiedDate
    @JsonIgnore
    private LocalDateTime updatedAt;

    @LastModifiedBy
    @JsonIgnore
    private String modifiedBy;
}
