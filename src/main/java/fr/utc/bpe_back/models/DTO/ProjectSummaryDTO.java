package fr.utc.bpe_back.models.DTO;

import fr.utc.bpe_back.models.projects.ProjectCategory;
import fr.utc.bpe_back.models.projects.ProjectStatus;

public interface ProjectSummaryDTO {
    String getTitle();
    String getDescription();

    Long getId();

    Boolean getRealised();

    ProjectCategory getCategory();

    ProjectStatus getStatus();

    Integer getVotantsCount();
}
