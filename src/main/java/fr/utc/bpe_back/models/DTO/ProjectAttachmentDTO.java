package fr.utc.bpe_back.models.DTO;

import fr.utc.bpe_back.models.projects.ProjectAttachmentType;

import java.net.URI;
import java.util.UUID;

public class ProjectAttachmentDTO {
    private String description;
    private ProjectAttachmentType type;
    private UUID id;
    private URI resourcePath;
    private String filename;
    private String fileType;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ProjectAttachmentType getType() {
        return type;
    }

    public void setType(ProjectAttachmentType type) {
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public URI getResourcePath() {
        return resourcePath;
    }

    public void setResourcePath(URI resourcePath) {
        this.resourcePath = resourcePath;
    }

    public String getFileType() {
        return filename.substring(filename.lastIndexOf(".") + 1);
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
