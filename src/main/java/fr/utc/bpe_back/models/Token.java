package fr.utc.bpe_back.models;

import fr.utc.bpe_back.models.users.User;
import jakarta.persistence.*;
import lombok.Data;
import java.util.Date;

/**
 * Refresh tokens.
 */
@Data
@Entity
@Table(name="tokens")
public class Token {
	@Id
	private String id;

	@OneToOne(targetEntity = User.class, optional = false)
	private User user;

	private long expiration;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getExpiration() {
		return expiration;
	}

	public void setExpiration(long expiration) {
		this.expiration = expiration;
	}

	public boolean isExpired() {
		Date now = new Date();
		return (now.getTime() > expiration);
	}
}
