package fr.utc.bpe_back.models.users;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}