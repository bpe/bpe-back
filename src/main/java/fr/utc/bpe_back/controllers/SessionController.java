package fr.utc.bpe_back.controllers;

import fr.utc.bpe_back.controllers.requests.TimelineStepRequest;
import fr.utc.bpe_back.models.DTO.SessionDTO;
import fr.utc.bpe_back.models.projects.ProjectStatus;
import fr.utc.bpe_back.models.projects.ProjectVote;
import fr.utc.bpe_back.models.sessions.Session;
import fr.utc.bpe_back.models.sessions.SessionTimelineStep;
import fr.utc.bpe_back.models.sessions.TimelineItem;
import fr.utc.bpe_back.models.users.Role;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.services.ProjectService;
import fr.utc.bpe_back.services.ProjectVoteService;
import fr.utc.bpe_back.services.SessionService;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/sessions")
public class SessionController {
    private final Logger logger = LoggerFactory.getLogger(SessionController.class);

    @Autowired
    private SessionService sessionService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ProjectVoteService projectVoteService;

    /**
     * Get all sessions
     *
     * @return Iterable of sessions
     */
    @GetMapping()
    public Iterable<Session> getSessions(@RequestParam(required = false, defaultValue = "false") Boolean includeDeleted, @AuthenticationPrincipal User user) {
        if (includeDeleted && (user == null || user.getRole() != Role.ROLE_ADMIN)) {
            return sessionService.getSessionsWithSteps(false);
        }
        return sessionService.getSessionsWithSteps(includeDeleted);
    }

    @PostMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> createSession(
            @RequestBody Session session
    ) {
        if (session.getSlug() == null || session.getName() == null) {
            return ResponseEntity.badRequest().body("Missing required parameters");
        }

        if (sessionService.getSessionBySlug(session.getSlug()) != null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Slug already in use");
        }

        logger.debug("Creating session {} ({})", session.getSlug(), session.getName());
        Session dbSession = new Session();
        try {
            dbSession.setSlug(session.getSlug());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Invalid slug");
        }
        dbSession.setName(session.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(sessionService.saveSession(dbSession));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Session> getSessionById(@PathVariable("id") Long id, @AuthenticationPrincipal User user) {
        Optional<Session> session = sessionService.getSessionWithSteps(id);
        if (session.isEmpty() || (session.get().getDeleted() && (user == null || user.getRole() != Role.ROLE_ADMIN))) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(session.get());
    }


    @PatchMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> updateSession(@PathVariable final Long id, @RequestBody SessionDTO session) {
        Optional<Session> s = sessionService.getSession(id);
        if (s.isPresent()) {
            Session currentSession = s.get();

            String name = session.getName();
            if (name != null && !Objects.equals(currentSession.getName(), name)) {
                currentSession.setName(name);
            }

            String slug = session.getSlug();
            if (slug != null && !Objects.equals(currentSession.getSlug(), slug)) {
                Session slugSession = sessionService.getSessionBySlug(slug);
                if (slugSession != null && !Objects.equals(slugSession.getId(), currentSession.getId())) {
                    return ResponseEntity.status(HttpStatus.CONFLICT).body("Slug already in use");
                }
                currentSession.setSlug(slug);
            }

            Boolean deleted = session.getDeleted();
            if (deleted != null && !Objects.equals(currentSession.getDeleted(), deleted)) {
                currentSession.setDeleted(deleted);
            }

            sessionService.saveSession(currentSession);
            return ResponseEntity.ok(currentSession);
        } else {
            return ResponseEntity.notFound().build();
        }
    }


    @DeleteMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public void deleteSession(@PathVariable Long id) {
        sessionService.deleteSession(id);
    }

    @PostMapping("/{id}/steps")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> createTimelineStep(
            @RequestBody TimelineStepRequest timelineStep, @PathVariable Long id) {
        if (timelineStep.getEnd() == null || timelineStep.getStart() == null || timelineStep.getTimelineItem() == null) {
            return ResponseEntity.badRequest().body("Missing required parameters");
        }

        // Check if session exists
        Optional<Session> session = sessionService.getSession(id);
        if (session.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Session not found");
        }

        // Check if timelineItem exists
        Optional<TimelineItem> timelineItem = sessionService.getTimelineItem(timelineStep.getTimelineItem());
        if (timelineItem.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Timeline item not found");
        }

        if (timelineStep.getStart().after(timelineStep.getEnd())) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Start cannot be after end...");
        }

        SessionTimelineStep sessionTimelineStep = new SessionTimelineStep();
        sessionTimelineStep.setSession(session.get());
        sessionTimelineStep.setTimelineItem(timelineItem.get());
        sessionTimelineStep.setStart(timelineStep.getStart());
        sessionTimelineStep.setEnd(timelineStep.getEnd());

        return ResponseEntity.status(HttpStatus.CREATED).body(sessionService.createTimelineStep(sessionTimelineStep));
    }

    @DeleteMapping("/{id}/steps/{stepName}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> deleteTimelineStepByName(
            @PathVariable Long id, @PathVariable String stepName) {
        Optional<Session> session = sessionService.getSession(id);
        if (session.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Session not found");
        }
        Optional<TimelineItem> timelineItem = sessionService.getTimelineItem(stepName);
        if (timelineItem.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Timeline item not found");
        }

        sessionService.deleteSessionTimelineStepsByName(session.get(), timelineItem.get());
        return ResponseEntity.noContent().build();
    }

    /**
     * Returns only validated projects for session except for admins
     *
     * @param id session id
     * @return Session Projects summary
     */
    @GetMapping("/{id}/projects")
    public ResponseEntity<?> getProjectsForSession(
            @PathVariable Long id,
            @RequestParam(required = false, defaultValue = "false") boolean full,
            @AuthenticationPrincipal final User user
    ) {
        Optional<Session> s = sessionService.getSession(id);
        if (s.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Session not found");
        }

        if (user != null && user.getRole() == Role.ROLE_ADMIN) {
            if (full) {
                return ResponseEntity.ok(projectService.getProjectsForSession(s.get()));
            }
            return ResponseEntity.ok(projectService.getProjectsSummaryForSession(s.get()));
        }

        // If session state VOTING is not yet started, return only authenticated user's projects
        Optional<SessionTimelineStep> sts = this.sessionService.getSessionTimelineStepByName(s.get(), "VOTING");
        if (sts.isEmpty() || sts.get().getStart().after(new java.util.Date())) {
            if (user == null) {
                return ResponseEntity.ok("[]");
            }
            return ResponseEntity.ok(projectService.getProjectsSummaryForSessionAndUser(user, s.get()));
        }

        return ResponseEntity.ok(projectService.getProjectsSummaryForSession(s.get(), ProjectStatus.ACCEPTED));
    }

    @GetMapping("/{id}/votes/{userId}")
    @Secured("ROLE_USER")
    @Transactional
    public ResponseEntity<?> getVotesForSession(
            @PathVariable Long id,
            @PathVariable Long userId,
            @AuthenticationPrincipal final User user
    ) {
        // Check if session exists
        Optional<Session> s = sessionService.getSession(id);
        if (s.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Session not found");
        }

        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User not authenticated");
        }

        // Check if user is authenticated user
        if (!Objects.equals(user.getId(), userId)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Well, that's not you...");
        }

        // Transform votes to array of project ids (longs)
        ArrayList<Long> votes = new ArrayList<>();
        Iterable<ProjectVote> userVotes = projectVoteService.getVotesForSessionAndUser(s.get(), user);
        for (ProjectVote vote : userVotes) {
            votes.add(vote.getProject().getId());
        }

        return ResponseEntity.ok(votes);
    }

}
