package fr.utc.bpe_back.controllers.requests;

import java.util.Date;

public class TimelineStepRequest {
    private final String timelineItem;
    private final Date start;
    private final Date end;

    public TimelineStepRequest(String timelineItem, Date start, Date end) {
        this.timelineItem = timelineItem;
        this.start = start;
        this.end = end;
    }

    public String getTimelineItem() {
        return timelineItem;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }
}
