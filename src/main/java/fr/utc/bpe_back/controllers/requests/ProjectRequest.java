package fr.utc.bpe_back.controllers.requests;

public class ProjectRequest {
    /**
     * ID of session
     */
    private Long session;

    /**
     * Project title
     */
    private String title;

    /**
     * Project short description
     */
    private String description;

    /**
     * Project details, HTML format
     */
    private String details;

    /**
     * Project category id
     */
    private Long category;

    /**
     * Owner user id
     */
    private Long owner;

    /**
     * User phone
     */
    private String phone;

    /**
     * Project workgroup (if given)
     */
    private String workgroup;

    /**
     * Project status
     */
    private String status;

    /**
     * Required budget
     */
    private Float budget;

    /**
     * Allocated budget by admins
     */
    private Float allocated_budget;

    /**
     * Is project realised
     */
    private Boolean realised;

    private Integer votants_count;

    public Long getSession() {
        return session;
    }

    public void setSession(Long session) {
        this.session = session;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkgroup() {
        return workgroup;
    }

    public void setWorkgroup(String workgroup) {
        this.workgroup = workgroup;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getBudget() {
        return budget;
    }

    public void setBudget(Float budget) {
        this.budget = budget;
    }

    public Float getAllocatedBudget() {
        return allocated_budget;
    }

    public void setAllocatedBudget(Float allocated_budget) {
        this.allocated_budget = allocated_budget;
    }

    public Boolean getRealised() {
        return realised;
    }

    public void setRealised(Boolean realised) {
        this.realised = realised;
    }

    public Integer getVotantsCount() {
        return votants_count;
    }

    public void setVotantsCount(Integer votants_count) {
        this.votants_count = votants_count;
    }
}
