package fr.utc.bpe_back.controllers;

import fr.utc.bpe_back.models.sessions.TimelineItem;
import fr.utc.bpe_back.services.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


@RestController
@CrossOrigin
@RequestMapping("/timeline-items")
public class TimelineItemController {
    private final Logger logger = LoggerFactory.getLogger(TimelineItemController.class);


    @Autowired
    private SessionService sessionService;

    @GetMapping
    public Iterable<TimelineItem> getTimelineItems() {
        return sessionService.getTimelineItems();
    }

    @GetMapping("/{id}")
    public ResponseEntity<TimelineItem> getTimelineItemById(@PathVariable("id") String id) {
        Optional<TimelineItem> timelineItem = sessionService.getTimelineItem(id);

        return timelineItem.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<?> createTimelineItem(@RequestBody TimelineItem timelineItem) {
        Optional <TimelineItem> dbTi = sessionService.getTimelineItem(timelineItem.getId());
        if (dbTi.isPresent()) {
            logger.info("Tried to override timeline item {}", timelineItem.getId());
            String errorMessage = "A session with ID <" + timelineItem.getId() + "> already exists.";
            return ResponseEntity.status(HttpStatus.CONFLICT).body(errorMessage);

        }

        logger.info("CREATE timeline item {}", timelineItem.getId());
        return ResponseEntity.ok(sessionService.createTimelineItem(timelineItem));
    }
}
