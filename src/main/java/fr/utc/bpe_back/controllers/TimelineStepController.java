package fr.utc.bpe_back.controllers;

import fr.utc.bpe_back.controllers.requests.TimelineStepRequest;
import fr.utc.bpe_back.models.sessions.SessionTimelineStep;
import fr.utc.bpe_back.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/timeline-steps")
public class TimelineStepController {
    @Autowired
    private SessionService sessionService;

    @PatchMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> updateTimelineStep(@PathVariable Long id, @RequestBody TimelineStepRequest timelineStepRequest) {
        Optional<SessionTimelineStep> step = sessionService.getSessionTimelineStep(id);
        if (step.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        SessionTimelineStep dbStep = step.get();
        if (timelineStepRequest.getStart() != null) {
            dbStep.setStart(timelineStepRequest.getStart());
        }
        if (timelineStepRequest.getEnd() != null) {
            dbStep.setEnd(timelineStepRequest.getEnd());
        }
        sessionService.saveSessionTimelineStep(dbStep);
        return ResponseEntity.ok().build();
    }
}
