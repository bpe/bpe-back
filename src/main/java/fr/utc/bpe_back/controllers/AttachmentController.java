package fr.utc.bpe_back.controllers;


import fr.utc.bpe_back.models.projects.ProjectAttachment;
import fr.utc.bpe_back.models.projects.ProjectAttachmentType;
import fr.utc.bpe_back.models.users.Role;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.services.ProjectAttachmentService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
@CrossOrigin
@RequestMapping("/attachments")
public class AttachmentController {
    private final Logger logger = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    ProjectAttachmentService projectAttachmentService;

    @SneakyThrows
    @GetMapping("/{id}")
    public ResponseEntity<?> getAttachment(@PathVariable("id") UUID uuid, HttpServletRequest request) {
        Optional<ProjectAttachment> projectAttachment = projectAttachmentService.getAttachmentById(uuid);
        if (projectAttachment.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Resource resource = projectAttachmentService.getAttachmentAsResource(projectAttachment.get());

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + projectAttachment.get().getFilename() + "\"")
                .cacheControl(CacheControl.maxAge(30, TimeUnit.MINUTES).cachePublic())
                .body(resource);
    }

    @GetMapping("/types")
    public ProjectAttachmentType[] getAttachmentTypes() {
        return ProjectAttachmentType.values();
    }

    @DeleteMapping("/{id}")
    @Secured("ROLE_USER")
    public ResponseEntity<?> deleteAttachment(@PathVariable("id") UUID uuid, @AuthenticationPrincipal User user) {
        Optional<ProjectAttachment> projectAttachment = projectAttachmentService.getAttachmentById(uuid);
        if (projectAttachment.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        // Check if user is allowed to delete this attachment (either admin or owner)
        if (user.getRole() != Role.ROLE_ADMIN && !user.getUsername().equals(projectAttachment.get().getProject().getOwner().getUsername())) {
            return ResponseEntity.status(403).build();
        }

        String filename = projectAttachment.get().getFilename();
        String projectTitle = projectAttachment.get().getProject().getTitle();
        projectAttachmentService.deleteAttachment(projectAttachment.get());
        logger.info("{} deleted attachment {} from project {}", user.getUsername(), filename, projectTitle);
        return ResponseEntity.noContent().build();
    }
}
