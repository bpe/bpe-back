package fr.utc.bpe_back.controllers;

import fr.utc.bpe_back.models.AppSettings;
import fr.utc.bpe_back.models.users.Role;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.services.AppSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/settings")
public class AppSettingsController {
    @Autowired
    private AppSettingsService appSettingsService;

    @GetMapping("/{id}")
    public ResponseEntity<?> getAppSettingById(@PathVariable("id") String id, @AuthenticationPrincipal User user) {
        AppSettings appSettings = appSettingsService.getAppSettingById(id);
        if (appSettings == null) {
            return ResponseEntity.notFound().build();
        }

        // If admin, OK
        if (user != null && user.getRole() == Role.ROLE_ADMIN) {
            return ResponseEntity.ok(appSettings);
        }

        // If no role is specified, anyone can access the setting value
        if (appSettings.getRole() == null) {
            return ResponseEntity.ok(appSettings.getValue());
        }

        // If user is not admin, but the setting is for users, OK
        if (appSettings.getRole() == Role.ROLE_USER) {
            return ResponseEntity.ok(appSettings);
        }

        return ResponseEntity.notFound().build();
    }

    @GetMapping("/")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> getAllAppSettings() {
        Iterable<AppSettings> appSettings = appSettingsService.getAllAppSettings();
        return ResponseEntity.ok(appSettings);
    }

    /**
     * Create an app setting
     */
    @PostMapping("/")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> createAppSetting(@RequestBody AppSettings appSettings) {
        // Check id is not null
        if (appSettings.getId() == null) {
            return ResponseEntity.badRequest().body("Id is required");
        }

        // Check if setting already exists
        if (appSettingsService.getAppSettingById(appSettings.getId()) != null) {
            return ResponseEntity.badRequest().body("Setting already exists, make a PUT request to update the value");
        }

        // Check if role is valid
        if (appSettings.getRole() != null && appSettings.getRole() != Role.ROLE_USER && appSettings.getRole() != Role.ROLE_ADMIN) {
            return ResponseEntity.badRequest().body("Role must be either ROLE_USER, ROLE_ADMIN or null");
        }
        AppSettings newAppSettings = appSettingsService.createAppSetting(appSettings);
        return ResponseEntity.ok(newAppSettings);
    }

    /**
     * Update an app setting. Only change the value field.
     */
    @PutMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> updateAppSetting(@PathVariable("id") String id, @RequestBody AppSettings appSettings) {
        AppSettings updatedAppSettings = appSettingsService.updateAppSetting(id, appSettings);
        if (updatedAppSettings == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedAppSettings);
    }

    /**
     * Delete an app setting
     */
    @DeleteMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> deleteAppSetting(@PathVariable("id") String id) {
        AppSettings appSettings = appSettingsService.getAppSettingById(id);
        if (appSettings == null) {
            return ResponseEntity.notFound().build();
        }
        appSettingsService.deleteAppSetting(appSettings);
        return ResponseEntity.noContent().build();
    }
}
