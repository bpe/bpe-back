package fr.utc.bpe_back.controllers;

import fr.utc.bpe_back.controllers.requests.ProjectRequest;
import fr.utc.bpe_back.exceptions.UnauthorizedAccessException;
import fr.utc.bpe_back.models.DTO.ProjectAttachmentDTO;
import fr.utc.bpe_back.models.DTO.ProjectCommentDTO;
import fr.utc.bpe_back.models.projects.*;
import fr.utc.bpe_back.models.sessions.Session;
import fr.utc.bpe_back.models.sessions.SessionTimelineStep;
import fr.utc.bpe_back.models.users.Role;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.services.*;
import jakarta.transaction.Transactional;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.StreamSupport;


@RestController
@CrossOrigin
@RequestMapping("/projects")
public class ProjectController {
    private final Logger logger = LoggerFactory.getLogger(ProjectController.class);


    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    ProjectAttachmentService projectAttachmentService;

    @Autowired
    ProjectVoteService projectVoteService;

    @Autowired
    EmailService mailService;

    @GetMapping("/{id}")
    public ResponseEntity<?> getProjectById(@PathVariable("id") Long id) throws UnauthorizedAccessException {
        Optional<Project> project = projectService.getProjectById(id);

        if (project.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(project.get());
    }

    @GetMapping("/")
    @Secured("ROLE_USER")
    public ResponseEntity<?> getProjectsForUser(@AuthenticationPrincipal User authenticatedUser, @RequestParam(value = "user") User u) {
        if (authenticatedUser == null || authenticatedUser.getRole() != Role.ROLE_ADMIN && !authenticatedUser.getUsername().equals(u.getUsername())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        return ResponseEntity.ok(projectService.getProjectsForUser(u));
    }


    @PostMapping
    @Secured("ROLE_USER")
    public ResponseEntity<?> createProject(@RequestBody ProjectRequest projectRequest, @AuthenticationPrincipal final User authenticatedUser) {
        /*
         * TODO WHEN ROLE_USER:
         *  - create slug ?
         */


        HashMap<String, String> errors = new HashMap<>();

        if (projectRequest.getTitle() == null || projectRequest.getTitle().isEmpty()) {
            errors.put("title", "Title cannot be null");
        }

        if (projectRequest.getDescription() == null || projectRequest.getDescription().isEmpty()) {
            errors.put("description", "Description cannot be null");
        }

        if (projectRequest.getBudget() == null || projectRequest.getBudget() <= 0) {
            errors.put("budget", "Budget must be a positive float");
        }

        if (projectRequest.getPhone() == null ||
                !projectRequest.getPhone().matches("^(?:(?:\\+|00)33|0)\\s*[1-9](?:[\\s.-]*\\d{2}){4}$")) {
            errors.put("phone", "Phone is invalid or null");
        }

        Optional<ProjectCategory> projectCategory = Optional.empty();
        if (projectRequest.getCategory() == null) {
            errors.put("project_category", "Project category cannot be null");
        } else {
            projectCategory = projectService.getProjectCategoryById(projectRequest.getCategory());
            if (projectCategory.isEmpty()) {
                errors.put("project_category", "Project category is invalid");
            }
        }

        Optional<User> owner = Optional.empty();
        if (projectRequest.getOwner() == null) {
            errors.put("owner", "Owner cannot be null");
        } else {
            owner = userService.loadUserById(projectRequest.getOwner());
            if (owner.isEmpty()) {
                errors.put("owner", "Owner is invalid");
            }
        }

        Optional<Session> session = Optional.empty();
        if (projectRequest.getSession() == null) {
            errors.put("session", "Session cannot be null");
        } else {
            session = sessionService.getSession(projectRequest.getSession());
            if (session.isEmpty()) {
                errors.put("session", "Session is invalid");
            }
        }

        // Check if active timeline item is DEPOSIT (except for admin)
        if (authenticatedUser.getRole() != Role.ROLE_ADMIN) {
            // Get current timeline item
            Optional<SessionTimelineStep> currentTimelineStep = sessionService.getCurrentSessionTimelineStep(session.get());
            if (currentTimelineStep.isEmpty()) {
                errors.put("session", "Session is not opened");
            } else if (!Objects.equals(currentTimelineStep.get().getTimelineItem().getId(), "DEPOSIT")) {
                errors.put("session", "Session is not opened");
            }
        }


        // If user is not admin, set owner to authenticated user
        if (authenticatedUser.getRole() != Role.ROLE_ADMIN && !authenticatedUser.getUsername().equals(owner.get().getUsername())) {
            owner = Optional.of(authenticatedUser);
        }

        // If owner is not admin, must have the "utc-etu" profile
        if (owner.isPresent() && owner.get().getRole() != Role.ROLE_ADMIN && !owner.get().getProfile().equals("utc-etu")) {
            errors.put("owner", "Owner must be an UTC student");
        }

        // if any error, return bad request
        if (!errors.isEmpty()) {
            return ResponseEntity.badRequest().body(errors);
        }


        String details = projectRequest.getDetails();

        Project _project = new Project();
        _project.setTitle(projectRequest.getTitle());
        _project.setDescription(projectRequest.getDescription());
        _project.setDetails(details);
        _project.setStatus(ProjectStatus.PENDING_VALIDATION);
        _project.setBudget(Math.round(projectRequest.getBudget() * 100.0f) / 100.0f);
        _project.setCategory(projectCategory.get());
        _project.setPhone(projectRequest.getPhone());
        _project.setWorkgroup(projectRequest.getWorkgroup());
        _project.setRealised(false);
        _project.setOwner(owner.get());
        _project.setSession(session.get());

        return ResponseEntity.status(HttpStatus.CREATED).body(projectService.createProject(_project));
    }

    @Secured("ROLE_USER")
    @PatchMapping("/{id}")
    public ResponseEntity<?> updateProject(@RequestBody ProjectRequest projectRequest, @PathVariable("id") Long projectId, @AuthenticationPrincipal final User authenticatedUser) throws UnauthorizedAccessException {
        // Get project from DB
        Optional<Project> project = projectService.getProjectById(projectId);
        if (project.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        Project dbProject = project.get();

        logger.info("User {} is trying to update project {}", authenticatedUser.getUsername(), dbProject.getTitle());

        if (authenticatedUser.getRole() != Role.ROLE_ADMIN) {
            // Check if active timeline item is DEPOSIT or (timeline item is PROJECT_EXAMINATION and project is in ACTION_NEEDED state)
            Optional<SessionTimelineStep> currentTimelineStep = sessionService.getCurrentSessionTimelineStep(project.get().getSession());
            if (currentTimelineStep.isEmpty()) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            if (!Objects.equals(currentTimelineStep.get().getTimelineItem().getId(), "DEPOSIT") &&
                    !(Objects.equals(currentTimelineStep.get().getTimelineItem().getId(), "PROJECT_EXAMINATION") && project.get().getStatus().equals(ProjectStatus.ACTION_NEEDED))) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }

            // Check if user is admin or owner
            if (!authenticatedUser.getUsername().equals(project.get().getOwner().getUsername())) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }

        // Check if project is not deleted
        if (project.get().getDeleted()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        // Update project title
        if (projectRequest.getTitle() != null) {
            project.get().setTitle(projectRequest.getTitle());
        }

        // Update project description
        if (projectRequest.getDescription() != null) {
            project.get().setDescription(projectRequest.getDescription());
        }

        // Update project budget
        if (projectRequest.getBudget() != null) {
            if (projectRequest.getBudget() <= 0) {
                return ResponseEntity.badRequest().body("Budget must be a positive float");
            }
            project.get().setBudget(Math.round(projectRequest.getBudget() * 100.0f) / 100.0f);
        }

        // Update project phone
        if (projectRequest.getPhone() != null) {
            if (!projectRequest.getPhone().matches("^(?:(?:\\+|00)33|0)\\s*[1-9](?:[\\s.-]*\\d{2}){4}$")) {
                return ResponseEntity.badRequest().body("Invalid phone number");
            }
            project.get().setPhone(projectRequest.getPhone());
        }

        // Update project workgroup
        if (projectRequest.getWorkgroup() != null) {
            project.get().setWorkgroup(projectRequest.getWorkgroup());
        }

        // Update project category
        if (projectRequest.getCategory() != null) {
            Optional<ProjectCategory> projectCategory = projectService.getProjectCategoryById(projectRequest.getCategory());
            if (projectCategory.isEmpty()) {
                return ResponseEntity.badRequest().body("Invalid project category");
            }
            project.get().setCategory(projectCategory.get());
        }

        // Update project details field
        if (projectRequest.getDetails() != null) {
            project.get().setDetails(projectRequest.getDetails());
        }

        // Update project status if admin only
        if (projectRequest.getStatus() != null && authenticatedUser.getRole() == Role.ROLE_ADMIN) {
            Optional<ProjectStatus> projectStatus = Arrays.stream(ProjectStatus.values()).filter(s -> s.name().equals(projectRequest.getStatus())).findFirst();
            if (projectStatus.isEmpty()) {
                return ResponseEntity.badRequest().body("Invalid project status");
            }
            project.get().setStatus(projectStatus.get());
        }

        // Update allocated budget if admin only
        if (projectRequest.getAllocatedBudget() != null && authenticatedUser.getRole() == Role.ROLE_ADMIN) {
            if (projectRequest.getAllocatedBudget() < 0) {
                return ResponseEntity.badRequest().body("Allocated budget must be a positive float");
            }
            project.get().setAllocatedBudget(Math.round(projectRequest.getAllocatedBudget() * 100.0f) / 100.0f);
        }

        // Save changes
        projectService.update(dbProject);

        return ResponseEntity.ok().build();
    }

    @SneakyThrows
    @Secured("ROLE_USER")
    @PostMapping("/{id}/attachments")
    public ResponseEntity<?> addAttachmentToProject(
            @RequestParam("attachment") MultipartFile attachment,
            @RequestParam("description") String description,
            @RequestParam("type") ProjectAttachmentType type,
            @PathVariable("id") Long projectId,
            @AuthenticationPrincipal final User authenticatedUser) {

        if (description.isEmpty()) {
            return ResponseEntity.badRequest().body("Invalid file description");
        }

        Optional<Project> project = projectService.getProjectById(projectId);
        if (project.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found");
        }

        logger.info("User {} is adding attachment {} to project {}", authenticatedUser.getUsername(), attachment.getName(), project.get().getTitle());

        if (authenticatedUser.getRole() != Role.ROLE_ADMIN) {
            // Check if active timeline item is DEPOSIT or (timeline item is PROJECT_EXAMINATION and project is in ACTION_NEEDED state)
            Optional<SessionTimelineStep> currentTimelineStep = sessionService.getCurrentSessionTimelineStep(project.get().getSession());
            if (currentTimelineStep.isEmpty()) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
            if (!Objects.equals(currentTimelineStep.get().getTimelineItem().getId(), "DEPOSIT") &&
                    !(Objects.equals(currentTimelineStep.get().getTimelineItem().getId(), "PROJECT_EXAMINATION") && project.get().getStatus().equals(ProjectStatus.ACTION_NEEDED))) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }

            // Check if user is admin or owner
            if (!authenticatedUser.getUsername().equals(project.get().getOwner().getUsername())) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }

            // User can't add attachment of "ACHIEVEMENT" type
            if (type.equals(ProjectAttachmentType.ACHIEVEMENT)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
        }

        String extension = ProjectAttachmentService.getFileExtension(Objects.requireNonNull(attachment.getOriginalFilename()));
        List<String> authorizedExtensions = Arrays.asList("xlsx", "pdf", "jpg", "png", "jpeg", "docx", "odt", "ods");
        if (!authorizedExtensions.contains(extension.toLowerCase())) {
            logger.info("{} tried to upload unauthorized file {}", authenticatedUser.getUsername(), attachment.getOriginalFilename());
            return ResponseEntity.badRequest().body("Unauthorized file extension");
        }

        ProjectAttachment projectAttachment = projectAttachmentService.storeFile(attachment, description, type, project.get());
        return ResponseEntity.created(projectAttachmentService.getAttachmentResourcePath(projectAttachment)).build();
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("{id}/comments")
    public ResponseEntity<?> addProjectComment(
            @PathVariable("id") Long id,
            @RequestBody ProjectComment projectComment,
            @AuthenticationPrincipal User authenticatedUser
    ) throws UnauthorizedAccessException {
        if (projectComment.getContent() == null || projectComment.getContent().isEmpty()) {
            return ResponseEntity.badRequest().body("Missing content");
        }

        Optional<Project> project = projectService.getProjectById(id);
        if (project.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found");
        }

        ProjectComment _projectComment = new ProjectComment();
        _projectComment.setProject(project.get());
        _projectComment.setUser(authenticatedUser);
        _projectComment.setContent(projectComment.getContent());
        _projectComment.setPublished(projectComment.getPublished());
        _projectComment = projectService.saveProjectComment(_projectComment);

        return ResponseEntity.ok(_projectComment);
    }

    @PatchMapping("{id}/comments/{commentId}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> updateProjectComment(
            @PathVariable("id") Long id,
            @PathVariable("commentId") Long commentId,
            @RequestBody ProjectComment projectComment,
            @AuthenticationPrincipal User authenticatedUser
    ) throws UnauthorizedAccessException {
        Optional<Project> project = projectService.getProjectById(id);
        if (project.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found");
        }

        Iterable<ProjectComment> dbProjectComment = projectService.getCommentsByProject(project.get(), false);
        // Check if comment exists
        Optional<ProjectComment> dbProjectCommentOptional = StreamSupport.stream(dbProjectComment.spliterator(), false).filter(c -> c.getId().equals(commentId)).findFirst();
        if (dbProjectCommentOptional.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Comment not found");
        }


        ProjectComment _projectComment = dbProjectCommentOptional.get();
//        _projectComment.setContent(projectComment.getContent());
        _projectComment.setPublished(projectComment.getPublished());
        _projectComment = projectService.saveProjectComment(_projectComment);

        return ResponseEntity.ok(_projectComment);
    }

    @GetMapping("{id}/attachments")
    public ResponseEntity<?> getProjectAttachments(@PathVariable("id") Long id) throws UnauthorizedAccessException {
        Optional<Project> project = projectService.getProjectById(id);
        if (project.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found");
        }

        Iterable<ProjectAttachment> attachments = projectAttachmentService.getAttachmentsByProject(project.get());
        List<ProjectAttachmentDTO> attachmentsRes = new ArrayList<>();
        attachments.forEach(a -> {
            ProjectAttachmentDTO _a = new ProjectAttachmentDTO();
            _a.setDescription(a.getDescription());
            _a.setResourcePath(projectAttachmentService.getAttachmentResourcePath(a));
            _a.setId(a.getId());
            _a.setType(a.getType());
            _a.setFilename(a.getFilename());
            attachmentsRes.add(_a);
        });

        return ResponseEntity.ok(attachmentsRes);
    }

    @GetMapping("{id}/comments")
    public ResponseEntity<?> getProjectComments(@AuthenticationPrincipal User u, @PathVariable("id") Long id) throws UnauthorizedAccessException {
        Optional<Project> project = projectService.getProjectById(id);
        if (project.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found");
        }

        if (u != null && u.getRole().equals(Role.ROLE_ADMIN)) {
            Iterable<ProjectComment> comments = this.projectService.getCommentsByProject(project.get(), false);
            List<ProjectCommentDTO> commentsRes = new ArrayList<>();
            comments.forEach(c -> {
                ProjectCommentDTO _c = new ProjectCommentDTO();
                _c.setContent(c.getContent());
                _c.setUser(c.getUser().getName());
                _c.setId(c.getId());
                _c.setDate(c.getCreatedAt());
                _c.setPublished(c.getPublished());
                commentsRes.add(_c);
            });
            return ResponseEntity.ok(commentsRes);
        }

        Iterable<ProjectCommentDTO> anonymousComments = this.getAnonymousCommentsByProject(project.get());

        return ResponseEntity.ok(anonymousComments);
    }

    private Iterable<ProjectCommentDTO> getAnonymousCommentsByProject(Project project) {
        Iterable<ProjectComment> comments = this.projectService.getCommentsByProject(project, true);
        List<ProjectCommentDTO> anonymousComments = new ArrayList<>();
        comments.forEach(c -> {
            ProjectCommentDTO _c = new ProjectCommentDTO();
            _c.setContent(c.getContent());
            _c.setUser("La commission");
            _c.setDate(c.getCreatedAt());
            anonymousComments.add(_c);
        });
        return anonymousComments;
    }

    @GetMapping("/categories")
    public Iterable<ProjectCategory> getProjectCategories() {
        return projectService.getProjectCategories();
    }

    @DeleteMapping("/{id}")
    @Secured("ROLE_USER")
    public ResponseEntity<?> deleteProject(@PathVariable("id") Long id, @AuthenticationPrincipal User u) throws UnauthorizedAccessException {
        // if user is not admin, check if owner is authenticated user
        if (u.getRole() != Role.ROLE_ADMIN) {
            Optional<Project> project = projectService.getProjectById(id);
            if (project.isEmpty()) {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found");
            }

            if (!project.get().getOwner().getUsername().equals(u.getUsername())) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized");
            }
        }

        Optional<Project> project = projectService.getProjectById(id);
        if (project.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found");
        }

        // If project is in PENDING_VALIDATION state, delete it directly otherwise set deleted flag to true
        if (project.get().getStatus().equals(ProjectStatus.PENDING_VALIDATION)) {
            projectService.deleteProject(project.get());
        } else {
            Project dbProject = project.get();
            dbProject.setDeleted(true);
            projectService.update(dbProject);
        }

        return ResponseEntity.noContent().build();
    }

    private Context seedMailContextFromProjectId(Long id) throws UnauthorizedAccessException {
        Optional<Project> project = projectService.getProjectById(id);
        if (project.isEmpty()) {
            throw new IllegalArgumentException("Project not found");
        }
        Project dbProject = project.get();
        Context context = new Context();
        context.setVariable("project", dbProject);
        context.setVariable("user", dbProject.getOwner());

        Optional<SessionTimelineStep> votingStep = this.sessionService.getSessionTimelineStepByName(dbProject.getSession(), "VOTING");
        if (votingStep.isEmpty()) {
            throw new IllegalArgumentException("No voting step found for this project session");
        }
        String voteStartStr = new SimpleDateFormat("EEEEE dd MMMMM yyyy",
                Locale.FRANCE).format(votingStep.get().getStart());
        context.setVariable("dateVote", voteStartStr);

        Optional<SessionTimelineStep> examinationStep = this.sessionService.getSessionTimelineStepByName(dbProject.getSession(), "PROJECT_EXAMINATION");
        if (examinationStep.isEmpty()) {
            throw new IllegalArgumentException("No examination step found for this project session");
        }
        String examinationEndStr = new SimpleDateFormat("EEEEE dd MMMMM yyyy",
                Locale.FRANCE).format(examinationStep.get().getEnd());
        context.setVariable("examinationEndDate", examinationEndStr);

        context.setVariable("comments", this.getAnonymousCommentsByProject(dbProject));

        return context;
    }

    @GetMapping("{id}/rendermail/{template}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> renderMailTemplate(
            @PathVariable("id") Long id,
            @PathVariable("template") String template
    ) throws UnauthorizedAccessException {
        Context context;
        try {
            context = seedMailContextFromProjectId(id);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }

        try {
            String rendered = mailService.renderTemplate(template, context);
            return ResponseEntity.ok(rendered);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error while rendering template");
        }
    }

    @PostMapping("{id}/sendresultmail")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> sendResultByMail(
            @PathVariable("id") Long id,
            @RequestParam(value = "dryrun", defaultValue = "false") Boolean dryrun
    ) {
        try {
            Context context = seedMailContextFromProjectId(id);
            // Template name depends on project status
            String template;
            switch (((Project) context.getVariable("project")).getStatus()) {
                case ACCEPTED -> template = "project_validated";
                case NOT_ACCEPTED -> template = "project_rejected";
                case ACTION_NEEDED -> template = "project_to_edit";
                default -> {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(
                            MessageFormat.format("No template for this project status ({0})", ((Project) context.getVariable("project")).getStatus())
                    );
                }
            }
            String rendered = mailService.renderTemplate(template, context);
            if (dryrun) {
                // Set content type to text/plain
                return ResponseEntity.ok().contentType(org.springframework.http.MediaType.TEXT_PLAIN).body(rendered);
            } else {
                mailService.sendEmail(
                        ((User) context.getVariable("user")).getEmail(),
                        "[BPE] Avis de la commission",
                        rendered,
                        true
                );
                return ResponseEntity.ok().build();
            }
        } catch (IllegalArgumentException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("An error occured during mail generation");
        }
    }

    /**
     * Vote for a project
     * @param id Project ID
     */
    @PostMapping("{id}/vote")
    @Secured("ROLE_USER")
    @Transactional
    public ResponseEntity<?> voteForProject(@PathVariable("id") Long id, @AuthenticationPrincipal User user) throws UnauthorizedAccessException {
//        Optional<Project> project = projectService.getProjectById(id);
        Optional<Project> project = projectService.getProjectByIdNoChecks(id);
        if (project.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found");
        }
        try {
            projectVoteService.voteForProject(project.get(), user);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
    }

    /**
     * Delete a vote for a project
     * @param id Project ID
     */
    @DeleteMapping("{id}/vote")
    @Secured("ROLE_USER")
    public ResponseEntity<?> deleteVoteForProject(@PathVariable("id") Long id, @AuthenticationPrincipal User user) throws UnauthorizedAccessException {
//        Optional<Project> project = projectService.getProjectById(id);
        Optional<Project> project = projectService.getProjectByIdNoChecks(id);
        if (project.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Project not found");
        }
        if (user == null) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized");
        }

        try {
            projectVoteService.deleteVoteForProject(project.get(), user);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(e.getMessage());
        }
    }
}
