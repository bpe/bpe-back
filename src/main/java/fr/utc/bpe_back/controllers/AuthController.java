package fr.utc.bpe_back.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.bpe_back.CAS.CasValidationResponse;
import fr.utc.bpe_back.exceptions.InvalidTokenException;
import fr.utc.bpe_back.models.AuthenticationTokenDTO;
import fr.utc.bpe_back.models.Token;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.services.JwtTokenService;
import fr.utc.bpe_back.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.naming.AuthenticationException;
import java.util.Collections;
import java.util.Date;

@RestController
@CrossOrigin
@RequestMapping("/auth")
public class AuthController {
    private final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Value("${auth.cas.server}")
    private String casServer;

    @Value("${auth.cas.service}")
    private String casService;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private UserService userService;

    private AuthenticationTokenDTO buildTokensForUser(User user, boolean includeRefreshToken) {
        // Generate token for the corresponding user
        AuthenticationTokenDTO resp = new AuthenticationTokenDTO();
        resp.setAccessToken(jwtTokenService.createAccessToken(user));

        if (includeRefreshToken) {
            Token refreshToken = jwtTokenService.createRefreshToken(user);
            resp.setRefreshToken(refreshToken.getId());
            resp.setExpiration(new Date(refreshToken.getExpiration()));
        }

        return resp;
    }

    public CasValidationResponse.CasServiceResponse.AuthenticationSuccess validateCasTicket(String ticket, String redirect) throws AuthenticationException {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        String fullServiceUrl = casService + "?redirect=" + redirect;

        String validationUrl = casServer + "/cas/p3/serviceValidate" + "?ticket=" + ticket + "&service=" + fullServiceUrl + "&format=json";

        ResponseEntity<String> response = restTemplate.exchange(
                validationUrl,
                HttpMethod.GET,
                entity,
                String.class
        );

        if (response.getStatusCode().is2xxSuccessful()) {
            String jsonResponse = response.getBody();
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                CasValidationResponse validationResponse = objectMapper.readValue(jsonResponse, CasValidationResponse.class);
                if (validationResponse.authenticationFailed()) {
                    logger.info(jsonResponse);
                    logger.info(objectMapper.writeValueAsString(validationResponse));
                    throw new AuthenticationException("CAS Authentication failed, " + validationResponse.getFailureCode());
                }
                return validationResponse.getProfile();

            } catch (JsonProcessingException e) {
                logger.warn(e.getMessage());
                throw new AuthenticationException("Failed to parse CAS response");
            }
        } else {
            throw new AuthenticationException("Failed to validate CAS ticket");
        }
    }

    @PostMapping("/refresh")
    public AuthenticationTokenDTO refreshAccessToken(
            @RequestBody AuthenticationTokenDTO refreshRequest
    ) throws InvalidTokenException {
        Token refreshToken = jwtTokenService.getRefreshToken(refreshRequest.getRefreshToken());
        User user = refreshToken.getUser();
        return buildTokensForUser(user, false);
    }

    @GetMapping("/login/cas")
    public ResponseEntity<Object> redirectCas() {
        String redirectUrl = casServer + "/cas/login?service=" + casService;
        return ResponseEntity
                .status(401)
                .header("Location", redirectUrl)
                .body(new Object() {
                    public final String redirect = redirectUrl;
                });
    }

    @PostMapping("/login/cas")
    public ResponseEntity<?> loginCas(@RequestBody TicketRequest ticketRequest) {
        // Validate ticket against CAS server
        try {
            final CasValidationResponse.CasServiceResponse.AuthenticationSuccess authResult = validateCasTicket(ticketRequest.getTicket(), ticketRequest.getRedirect());
            User user;
            try {
                user = userService.loadUserByUsername(authResult.getUsername());
                // Check if the account profile has changed
                if ((user.getProfile() == null && authResult.getAccountProfile() != null) || !user.getProfile().equals(authResult.getAccountProfile())) {
                    user.setProfile(authResult.getAccountProfile());
                    user = userService.updateUser(user.getId(), user);
                }
            } catch (UsernameNotFoundException e) {
                user = userService.createUser(
                        authResult.getUsername(),
                        authResult.getName(),
                        authResult.getEmail(),
                        authResult.getAccountProfile()
                );
            }

            // generate token
            AuthenticationTokenDTO tokenDTO = buildTokensForUser(user, true);

            return ResponseEntity.status(HttpStatus.OK).body(tokenDTO);

        } catch (AuthenticationException e) {
            logger.warn("Failed to authenticate user, " + e.getMessage());
            return ResponseEntity.status(401).body("Authentication failed");
        }
    }

    private static class TicketRequest {
        private String ticket;

        private String redirect;

        public String getRedirect() {
            return redirect;
        }

        public String getTicket() {
            return ticket;
        }
    }

    @PostMapping("/me")
    @Secured("ROLE_USER")
    public User getCurrentUser(@AuthenticationPrincipal final User user) {
        return userService.loadUserByUsername(user.getUsername());
    }

    @DeleteMapping("/tokens/{id}")
    public ResponseEntity<?> removeRefreshToken(@PathVariable String id) {
        jwtTokenService.removeRefreshToken(id);
        String redirectUrl = casServer + "/cas/logout";
        return ResponseEntity.ok(new Object() {
            public final String redirect = redirectUrl;
        });
    }
}
