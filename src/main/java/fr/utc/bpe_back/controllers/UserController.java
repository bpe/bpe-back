package fr.utc.bpe_back.controllers;

import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/users")
@Secured("ROLE_USER")
public class UserController {
    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserService userService;

    @GetMapping
    @Secured("ROLE_ADMIN")
    public Iterable<User> getUsers() {
        return userService.getUsers();
    }

    // Get user by id
    @GetMapping("/{id}")
    @ResponseBody
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> getUser(@PathVariable final Long id) {
        Optional<User> userOptional = userService.loadUserById(id);
        if (userOptional.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(userOptional.get());
    }


    @PostMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> createUser(@RequestBody User user) {
        if (user.getUsername() == null || user.getEmail() == null || user.getName() == null || user.getProfile() == null) {
            return ResponseEntity.badRequest().body("Missing required parameters");
        }

        // Check if user exists
        try {
            userService.loadUserByUsername(user.getUsername());
            return ResponseEntity.status(HttpStatus.CONFLICT).body("User already exists");
        } catch (UsernameNotFoundException e) {
            return ResponseEntity.status(HttpStatus.CREATED).body(
                    userService.createUser(user.getUsername(), user.getName(), user.getEmail(), user.getProfile())
            );
        }
    }


    //TODO : use JSON Patch instead of this **xxx** (https://tools.ietf.org/html/rfc6902)
    @PatchMapping("/{id}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @RequestBody User user, @AuthenticationPrincipal User authenticatedUser) {
        logger.info("Updating user {}, by {}", id, authenticatedUser.getUsername());

        User userData = new User();
        userData.setRole(user.getRole());

        return ResponseEntity.ok(userService.updateUser(id, userData));

    }
}