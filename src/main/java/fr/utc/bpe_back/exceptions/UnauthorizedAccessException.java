package fr.utc.bpe_back.exceptions;

public class UnauthorizedAccessException extends Exception {
	public UnauthorizedAccessException(String msg) {
		super(msg);
	}
}
