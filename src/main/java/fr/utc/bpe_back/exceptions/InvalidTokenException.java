package fr.utc.bpe_back.exceptions;

import java.io.Serial;

public class InvalidTokenException extends Exception {
	@Serial
	private static final long serialVersionUID = -1114799428998802896L;

	public InvalidTokenException(String msg) {
		super(msg);
	}
}
