package fr.utc.bpe_back.seeders;

import fr.utc.bpe_back.models.projects.ProjectCategory;
import fr.utc.bpe_back.models.sessions.TimelineItem;
import fr.utc.bpe_back.models.users.Role;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.repositories.projects.ProjectCategoryRepository;
import fr.utc.bpe_back.repositories.sessions.TimelineItemRepository;
import fr.utc.bpe_back.repositories.UserRepository;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


@Component
public class DbSeeder {
    @Value("${dbseeder.directory}")
    private String seederDirectory;
    private final Logger logger = LoggerFactory.getLogger(DbSeeder.class);

    @Autowired
    TimelineItemRepository timelineItemRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProjectCategoryRepository projectCategoryRepository;

    @Autowired
    DataSource dataSource;

    @EventListener
    public void seed(ContextRefreshedEvent ignoredEvent) {
        try {
            logger.info("Seed db if needed");
            seedAdmin();
            seedTimelineItems();
//            createProjectsView();
//            seedProjectCategories();
        } catch (Exception e) {
            logger.error("Failed to seed db, {}", e.getMessage());
        }

    }

    private void seedAdmin() {
        logger.info("[Admin users] Seed start");
        if (userRepository.getUserByUsername("jumelloi").isEmpty()) {
            User jumel = new User();
            jumel.setRole(Role.ROLE_ADMIN);
            jumel.setEmail("loic.jumel@utc.fr");
            jumel.setName("Loïc JUMEL");
            jumel.setProfile("utc-pers");
            jumel.setUsername("jumelloi");
            jumel.setActive(true);
            userRepository.save(jumel);
            logger.info("[Admin users], created {}", jumel.getUsername());
        }
        logger.info("[Admin users] Seed end");
    }

    @SneakyThrows
    private void seedTimelineItems() {
        logger.info("[Timeline items] Seed start");

        String csvFile = seederDirectory + "/timeline-items.csv";

        ArrayList<HashMap<String, String>> dataList = CSVToHashMap.read(csvFile);

        // Print the data stored in the List of HashMaps
        assert dataList != null;
        for (Map<String, String> dataMap : dataList) {
            if (timelineItemRepository.findById(dataMap.get("id")).isEmpty()) {
                TimelineItem ti = new TimelineItem();
                ti.setId(dataMap.get("id"));
                ti.setName(dataMap.get("name"));
                timelineItemRepository.save(ti);
                logger.info("[Timeline items], created {}", ti.getId());
            }
        }
        logger.info("[Timeline items] Seed end");
    }

    @SneakyThrows
    private void seedProjectCategories() {
        logger.info("[Project Categories] Seed start");

        String csvFile = seederDirectory + "/project-categories.csv";

        ArrayList<HashMap<String, String>> dataList = CSVToHashMap.read(csvFile);

        // Print the data stored in the List of HashMaps
        assert dataList != null;
        for (Map<String, String> dataMap : dataList) {
            if (projectCategoryRepository.findByName(dataMap.get("name")).isEmpty()) {
                ProjectCategory pc = new ProjectCategory();
                pc.setName(dataMap.get("name"));
                projectCategoryRepository.save(pc);
                logger.info("[Project Categories], created {}", pc.getName());
            }
        }
        logger.info("[Project Categories] Seed end");
    }
}