package fr.utc.bpe_back.seeders;


import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class CSVToHashMap {
    public static ArrayList<HashMap<String, String>> read(String csvFile) throws IOException, CsvValidationException {
        ArrayList<HashMap<String, String>> dataList = new ArrayList<>();

        CSVReader reader = new CSVReader(new FileReader(csvFile));
        String[] header = reader.readNext(); // Read the header line

        if (header == null) {
            // Handle empty CSV file or error
            return null;
        }

        String[] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            if (nextLine.length != header.length) {
                // Handle line with incorrect number of columns
                continue;
            }

            HashMap<String, String> dataMap = new HashMap<>();
            for (int i = 0; i < header.length; i++) {
                String key = header[i];
                String value = nextLine[i];
                dataMap.put(key, value);
            }
            dataList.add(dataMap);
        }

        return dataList;
    }
}
