package fr.utc.bpe_back.services;

import fr.utc.bpe_back.models.users.Role;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.debug("Loading user details for {}", username);
        return userRepository.getUserByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    /**
     * Create a new user in database.
     *
     * @param username the username
     * @return the new user
     */
    public User createUser(String username, String name, String email, String profile) {
        logger.info("Creating user {} in local database (profile={})", username, profile);

        try {
            User user = new User();
            user.setActive(true);
            user.setUsername(username);
            user.setEmail(email);
            user.setName(name);
            user.setProfile(profile);
            user.setRole(Role.ROLE_USER);
            return userRepository.save(user);
        } catch (Exception e) {
            throw new AuthenticationServiceException("Unable to create user " + username, e);
        }
    }

    public Iterable<User> getUsers() {
        return userRepository.findAllByOrderByIdAsc();
    }

    public Optional<User> loadUserById(Long id) {
        return userRepository.findById(id);
    }

    public User updateUser(Long id, User userData) {
        // For now, only the role can be updated
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }

        User user = userOptional.get();
        user.setRole(userData.getRole());
        user.setProfile(userData.getProfile());
        return userRepository.save(user);
    }
}
