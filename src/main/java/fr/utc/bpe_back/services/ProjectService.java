package fr.utc.bpe_back.services;

import fr.utc.bpe_back.exceptions.UnauthorizedAccessException;
import fr.utc.bpe_back.models.DTO.ProjectSummaryDTO;
import fr.utc.bpe_back.models.projects.Project;
import fr.utc.bpe_back.models.projects.ProjectCategory;
import fr.utc.bpe_back.models.projects.ProjectComment;
import fr.utc.bpe_back.models.projects.ProjectStatus;
import fr.utc.bpe_back.models.sessions.Session;
import fr.utc.bpe_back.models.sessions.SessionTimelineStep;
import fr.utc.bpe_back.models.users.Role;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.repositories.projects.ProjectAttachmentRepository;
import fr.utc.bpe_back.repositories.projects.ProjectCategoryRepository;
import fr.utc.bpe_back.repositories.projects.ProjectCommentRepository;
import fr.utc.bpe_back.repositories.projects.ProjectRepository;
import jakarta.transaction.Transactional;
import org.jsoup.safety.Safelist;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

import org.jsoup.Jsoup;
import org.thymeleaf.context.Context;

@Service
public class ProjectService {
    private final Logger logger = LoggerFactory.getLogger(ProjectService.class);

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    ProjectCommentRepository projectCommentRepository;

    @Autowired
    ProjectCategoryRepository projectCategoryRepository;

    @Autowired
    ProjectAttachmentRepository projectAttachmentRepository;

    @Autowired
    ProjectVoteService projectVoteService;

    @Autowired
    SessionService sessionService;

    @Autowired
    EmailService emailService;

    public Iterable<Project> getProjects() {
        return projectRepository.findAll();
    }

    public Iterable<Project> getProjectsForSession(Session s) {
        return projectRepository.getProjectsBySession(s);
    }

    public Iterable<Project> getProjectsForSession(Session s, ProjectStatus status) {
        return projectRepository.getProjectsBySessionAndStatus(s, status);
    }

    public Iterable<ProjectSummaryDTO> getProjectsSummaryForSession(Session s) {
        return projectRepository.getProjectsSummaryBySession(s);
    }

    public Iterable<ProjectSummaryDTO> getProjectsSummaryForSession(Session s, ProjectStatus status) {
        return projectRepository.getProjectsSummaryBySessionAndStatus(s, status);
    }

    public Iterable<ProjectSummaryDTO> getProjectsSummaryForSessionAndUser(User user, Session session) {
        return projectRepository.getProjectsSummaryBySessionAndUser(session, user);
    }

    public Iterable<ProjectComment> getCommentsByProject(Project project, Boolean publishedOnly) {
        if (publishedOnly) {
            return projectCommentRepository.getPublishedCommentsByProject(project);
        }
        return projectCommentRepository.getCommentsByProject(project);
    }

    public Optional<Project> getProjectById(Long id) throws UnauthorizedAccessException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = null;
        if (authentication != null) {
            user = (User) authentication.getPrincipal();
        }

        Optional<Project> project = projectRepository.findById(id);

        if (project.isEmpty()) {
            return project;
        }

        project.get().setSession(project.get().getSession());
        project.get().setVotantsCount(projectVoteService.getVoteCountByProject(project.get()));

        // If admin -> return project
        if (user != null && user.getRole().equals(Role.ROLE_ADMIN)) {
            return project;
        }

        // If session state REALIZATION is not yet started, remove allocated budget
        Optional<SessionTimelineStep> sts = this.sessionService.getSessionTimelineStepByName(project.get().getSession(), "REALIZATION");

        if (sts.isEmpty() || sts.get().getStart().before(new java.util.Date())) {
            project.get().setAllocatedBudget(null);
        }

        // If owner is authenticated user, go
        if (user != null && user.getId().equals(project.get().getOwner().getId())) {
            return project;
        }

        // If session state VOTING is not yet started, reject access
        sts = this.sessionService.getSessionTimelineStepByName(project.get().getSession(), "VOTING");
        if (sts.isEmpty() || sts.get().getStart().after(new java.util.Date())) {
            throw new UnauthorizedAccessException("You are not allowed to access this project.");
        }

        // If project is in accepted state, go with reduced data
        if (project.get().getStatus().equals(ProjectStatus.ACCEPTED)) {
            Project _p = project.get();
            User _u = new User();
            _u.setName(_p.getOwner().getName());
            _p.setOwner(_u);
            _p.setPhone(null);
            return Optional.of(_p);
        }


        // Default drop by throwing unauthorized access exception
        throw new UnauthorizedAccessException("You are not allowed to access this project.");
    }

    public Optional<ProjectCategory> getProjectCategoryById(Long id) {
        return projectCategoryRepository.findById(id);
    }

    public Project createProject(Project project) {
        Project dbProject = projectRepository.save(project);
        Context ctx = new Context();
        ctx.setVariable("project", dbProject);
        ctx.setVariable("user", dbProject.getOwner());
        emailService.sendEmail(dbProject.getOwner().getEmail(), "[BPE] Projet déposé", "project_created", ctx, true);
        return dbProject;
    }

    public ProjectComment saveProjectComment(ProjectComment projectComment) {
        return projectCommentRepository.save(projectComment);
    }

    public Iterable<ProjectCategory> getProjectCategories() {
        return projectCategoryRepository.findAll();
    }

    public void update(Project dbProject) {
        // Sanitize details field using Jsoup
        // dbProject.setDetails(Jsoup.clean(dbProject.getDetails(), Whitelist.basic()));
        if (dbProject.getDetails() != null) {
            dbProject.setDetails(Jsoup.clean(dbProject.getDetails(), Safelist.relaxed().addAttributes("p", "br", "strong", "em", "u", "ol", "li", "ul", "h1", "h2", "h3")));
        }
        User authUser = SecurityContextHolder.getContext().getAuthentication() != null ? (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal() : null;
        assert authUser != null;
        logger.info("User {} is updating project {}", authUser.getName(), dbProject.getTitle());

        // Send mail on project update if project is in ACTION_NEEDED state
        if (dbProject.getStatus().equals(ProjectStatus.ACTION_NEEDED) && authUser.getRole() != Role.ROLE_ADMIN) {
            Context ctx = new Context();
            ctx.setVariable("project", dbProject);
            ctx.setVariable("user", dbProject.getOwner());
            emailService.sendEmail(dbProject.getOwner().getEmail(), "[BPE] Projet modifié", "project_updated", ctx, false);
        }

        projectRepository.save(dbProject);
    }

    @Transactional
    public void deleteProject(Project project) {
        logger.info("User {} is deleting project {}", SecurityContextHolder.getContext().getAuthentication().getName(), project.getTitle());
        // Delete all comments
        projectCommentRepository.deleteByProject(project);
        // Delete all attached files
        projectAttachmentRepository.deleteByProject(project);
        // Delete all votes (should not be necessary)
        projectVoteService.deleteByProject(project);
        // Delete project
        projectRepository.delete(project);
    }

    public Object getProjectsForUser(User u) {
        return projectRepository.getProjectsByUser(u);
    }

    public Optional<Project> getProjectByIdNoChecks(Long id) {
        return projectRepository.findById(id);
    }
}