/**
 * Derived from HCMDB, UTC
 */
package fr.utc.bpe_back.services;

import fr.utc.bpe_back.exceptions.InvalidTokenException;
import fr.utc.bpe_back.models.AuthenticationToken;
import fr.utc.bpe_back.models.Token;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.repositories.TokenRepository;
import fr.utc.bpe_back.repositories.UserRepository;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class JwtTokenService {
    private final Logger logger = LoggerFactory.getLogger(JwtTokenService.class);

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Value("${token.secret}")
    private String secret;

    @Value("${token.validity}")
    private int tokenValidity;

    @Value("${token.allowed_clock_skew:0}")
    private int allowedClockSkew;

    // Secret key used for token signature
    private SecretKey secretKey;

    // Token cache
    private final Map<String, AuthenticationToken> tokenCache = new HashMap<>();

    @PostConstruct
    private void init() {
        secretKey = Keys.hmacShaKeyFor(secret.getBytes());
        logger.info("allowed clock skew for JWT: {} seconds", allowedClockSkew);
    }

    public Token createRefreshToken(User user) {
        long now = new Date().getTime();
        long sessionTime = 86400;

        Token token = new Token();
        token.setId(UUID.randomUUID().toString());
        token.setUser(user);
        token.setExpiration(now + (sessionTime * 1000));
        return tokenRepository.save(token);
    }

    public String createAccessToken(User user) {
        Date now = new Date();

        Claims claims = new DefaultClaims();
        claims.put("uid", user.getId());
        claims.put("username", user.getUsername());
        claims.setIssuedAt(now);
        claims.setNotBefore(claims.getIssuedAt());
        claims.setExpiration(new Date(now.getTime() + tokenValidity * 1000L));

        // Add privileges
        List<String> privileges =
                user.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toList());

        claims.put("privileges", privileges);

        return Jwts.builder()
                .signWith(secretKey, SignatureAlgorithm.HS512)
                .setClaims(claims)
                .compact();
    }

    // Create an access token based on a refresh token
    public String createAccessToken(Token refreshToken) {
        User user = userRepository.getUserById(refreshToken.getUser().getId()).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return createAccessToken(user);
    }

    public Jws<Claims> validateToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .setAllowedClockSkewSeconds(allowedClockSkew)
                .build()
                .parseClaimsJws(token);
    }

    public User getUserFromClaims(Jws<Claims> claims) {

        Claims body = claims.getBody();
        String username = (String) body.get("username");

        return userRepository.getUserByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    public Authentication authenticate(String accessToken) {
        AuthenticationToken auth = tokenCache.get(accessToken);

        if (auth == null) {
            Jws<Claims> claims = validateToken(accessToken);
            User user = getUserFromClaims(claims);
            auth = new AuthenticationToken(user, user.getAuthorities(), claims.getBody().getExpiration());
            tokenCache.put(accessToken, auth);
        } else {
            // Check expiration date of cached token
            Date now = new Date();

            if (now.after(auth.getExpiration()))
                throw new JwtException("Expired access token");
        }

        SecurityContextHolder.getContext().setAuthentication(auth);
        return auth;
    }

    public void removeRefreshToken(String refreshTokenId) {
        Optional<Token> optToken = tokenRepository.findById(refreshTokenId);

        optToken.ifPresent(token -> tokenRepository.delete(token));
    }

    public Token getRefreshToken(String refreshTokenId) throws InvalidTokenException {
        if (refreshTokenId == null)
            throw new InvalidTokenException("Null token");

        Optional<Token> optToken = tokenRepository.findById(refreshTokenId);

        if (optToken.isEmpty())
            throw new InvalidTokenException("Invalid or expired token");

        Token refreshToken = optToken.get();

        if (refreshToken.isExpired()) {
            logger.info("Refresh token {} is expired, removing", refreshTokenId);
            tokenRepository.delete(refreshToken);
            throw new InvalidTokenException("Expired token");
        }

        return refreshToken;
    }

    @Scheduled(fixedRate = 60 * 1000)
    public void cleanup() {
        logger.debug("Cleaning up old refresh tokens");
        tokenRepository.deleteExpiredTokens(new Date().getTime());
    }
}
