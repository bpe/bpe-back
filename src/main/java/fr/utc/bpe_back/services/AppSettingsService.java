package fr.utc.bpe_back.services;

import fr.utc.bpe_back.models.AppSettings;
import fr.utc.bpe_back.repositories.AppSettingsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AppSettingsService {
    Logger logger = LoggerFactory.getLogger(AppSettingsService.class);

    @Autowired
    private AppSettingsRepository appSettingsRepository;

    public AppSettings getAppSettingById(String id) {
        return appSettingsRepository.findById(id).orElse(null);
    }

    public void deleteAppSetting(AppSettings appSettings) {
        logger.info("Deleting app setting with id: " + appSettings.getId());
        appSettingsRepository.delete(appSettings);
    }

    public Iterable<AppSettings> getAllAppSettings() {
        return appSettingsRepository.findAll();
    }

    public AppSettings updateAppSetting(String id, AppSettings appSettings) {
        logger.info("Updating app setting with id: " + id);
        AppSettings appSettingsToUpdate = appSettingsRepository.findById(id).orElse(null);
        if (appSettingsToUpdate == null) {
            return null;
        }
        appSettingsToUpdate.setValue(appSettings.getValue());
        return appSettingsRepository.save(appSettingsToUpdate);
    }

    public AppSettings createAppSetting(AppSettings appSettings) {
        logger.info("Creating app setting with id: " + appSettings.getId());
        return appSettingsRepository.save(appSettings);
    }
}
