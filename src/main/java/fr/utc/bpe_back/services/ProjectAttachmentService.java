package fr.utc.bpe_back.services;

import fr.utc.bpe_back.exceptions.FileStorageException;
import fr.utc.bpe_back.models.projects.Project;
import fr.utc.bpe_back.models.projects.ProjectAttachment;
import fr.utc.bpe_back.models.projects.ProjectAttachmentType;
import fr.utc.bpe_back.models.users.Role;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.repositories.projects.ProjectAttachmentRepository;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.*;

@Service
public class ProjectAttachmentService {
    private final Path fileUploadDirectory;

    Logger logger = LoggerFactory.getLogger(ProjectAttachmentService.class);

    @Autowired
    ProjectAttachmentRepository projectAttachmentRepository;

    public ProjectAttachmentService(@Value("${files.upload.directory}") String fileUploadDirectory) {
        try {
            this.fileUploadDirectory = Path.of(fileUploadDirectory);
            logger.info("Creating file upload directory {}", fileUploadDirectory);
            Files.createDirectories(this.fileUploadDirectory);
        } catch (Exception ex) {
            throw new FileStorageException("An error occured while creating file upload directory", ex);
        }
    }

    public ProjectAttachment storeFile(MultipartFile file, String description, ProjectAttachmentType type, Project project) {
        // Create project attachment
        ProjectAttachment projectAttachment = new ProjectAttachment();
        projectAttachment.setDescription(description);
        projectAttachment.setType(type);
        projectAttachment.setProject(project);

        // Remove unicode characters out of range 0-255 from filename
        projectAttachment.setFilename(Objects.requireNonNull(file.getOriginalFilename()).replaceAll("[^\\u0000-\\u00FF]", "_"));

        try {
            projectAttachmentRepository.save(projectAttachment);

            Path uploadPath = Path.of(this.fileUploadDirectory + "/" + projectAttachment.getProject().getId().toString());

            try {
                Files.createDirectories(uploadPath);
            } catch (Exception ex) {
                throw new FileStorageException("An error occured while creating file upload directory", ex);
            }

            String extension = getFileExtension(Objects.requireNonNull(file.getOriginalFilename()));
            Path targetLocation = uploadPath.resolve(projectAttachment.getId().toString() + "." + extension);

            if (Arrays.asList("png", "jpg", "jpeg").contains(extension)) {
                byte[] compressedImageBytes = compressImage(file);
                Files.write(targetLocation, compressedImageBytes);
            } else {
                Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            }


            return projectAttachment;
        } catch (IOException ex) {
            projectAttachmentRepository.delete(projectAttachment);
            throw new FileStorageException("Could not store file ", ex);
        }
    }

    public static String getFileExtension(String fileName) {
        int dotIndex = fileName.lastIndexOf(".");
        if (dotIndex != -1 && dotIndex < fileName.length() - 1) {
            return fileName.substring(dotIndex + 1);
        }
        return "";
    }

    public Resource getAttachmentAsResource(ProjectAttachment projectAttachment) throws FileNotFoundException, MalformedURLException {
        Path uploadDir = Path.of(this.fileUploadDirectory + "/" + projectAttachment.getProject().getId().toString());
        Path filePath = uploadDir.resolve(projectAttachment.getId().toString() + "." + getFileExtension(projectAttachment.getFilename())).normalize();
        Resource resource = new UrlResource(filePath.toUri());
        if (resource.exists()) {
            return resource;
        } else {
            throw new FileNotFoundException("File not found " + projectAttachment.getId());
        }
    }

    public Optional<ProjectAttachment> getAttachmentById(UUID uuid) {
        return projectAttachmentRepository.findById(uuid);
    }

    @SneakyThrows
    public void deleteAttachment(ProjectAttachment projectAttachment) {
        // Delete file from storage
        Path uploadDir = Path.of(this.fileUploadDirectory + "/" + projectAttachment.getProject().getId().toString());
        Path filePath = uploadDir.resolve(projectAttachment.getId().toString() + "." + getFileExtension(projectAttachment.getFilename())).normalize();
        Files.deleteIfExists(filePath);


        // Delete ProjectAttachment
        projectAttachmentRepository.delete(projectAttachment);
    }

    private byte[] compressImage(MultipartFile image) throws IOException {

        InputStream inputStream = image.getInputStream();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        float imageQuality = 0.3f;

        // Create the buffered image
        BufferedImage bufferedImage = ImageIO.read(inputStream);

        // Get image writers
        Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByFormatName(getFileExtension(Objects.requireNonNull(image.getOriginalFilename()))); // Input your Format Name here

        if (!imageWriters.hasNext())
            throw new IllegalStateException("Image writer for compression not found!!");

        ImageWriter imageWriter = imageWriters.next();
        ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(outputStream);
        imageWriter.setOutput(imageOutputStream);

        ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();

        // Set the compress quality metrics
        imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        imageWriteParam.setCompressionQuality(imageQuality);

        // Compress and insert the image into the byte array.
        imageWriter.write(null, new IIOImage(bufferedImage, null, null), imageWriteParam);

        byte[] imageBytes = outputStream.toByteArray();

        // close all streams
        inputStream.close();
        outputStream.close();
        imageOutputStream.close();
        imageWriter.dispose();


        return imageBytes;
    }

    public Iterable<ProjectAttachment> getAttachmentsByProject(Project project) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = null;
        if (authentication != null) {
            user = (User) authentication.getPrincipal();
        }

        boolean userIsOwner = user != null && user.getId().equals(project.getOwner().getId());

        // Non authenticated users can see only part of attachments
        // As well as authenticated users that are not project owner
        if (user != null && user.getRole().equals(Role.ROLE_ADMIN) || userIsOwner) {
            return projectAttachmentRepository.getAttachmentsByProject(project);
        }

        return projectAttachmentRepository.getAttachmentsExceptBudgetByProject(project);
    }

    public URI getAttachmentResourcePath(ProjectAttachment a) {
        return URI.create("/attachments/" + a.getId());
    }
}
