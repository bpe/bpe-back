package fr.utc.bpe_back.services;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.logging.Logger;


@Service
public class EmailService {
    Logger logger = Logger.getLogger(EmailService.class.getName());

    @Autowired
    private final JavaMailSender javaMailSender;

    @Autowired
    private final TemplateEngine templateEngine;


    @Value("${spring.mail.properties.mail.from}")
    public String from;

    @Value("${spring.mail.properties.mail.template-base}")
    private String templateBase;

    public EmailService(JavaMailSender javaMailSender, TemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
    }

    @SneakyThrows
    public void sendEmail(String to, String subject, String body, Boolean adminCopy) {
        MimeMessageHelper messageHelper
                = new MimeMessageHelper(javaMailSender.createMimeMessage());

        // Logging
        logger.info("Sending email to " + to + " with subject " + subject);

        messageHelper.setFrom(from);
        messageHelper.setTo(to);
        messageHelper.setSubject(subject);
        messageHelper.setText(body, true);

        if (adminCopy) {
            messageHelper.setCc(from);
        }

        // Sending the mail
        javaMailSender.send(messageHelper.getMimeMessage());
    }

    /**
     * Send an email with the given template and context
     */
    public void sendEmail(String to, String subject, String templateName, Context context, Boolean adminCopy) {
        String body = renderTemplate(templateName, context);
        sendEmail(to, subject, body, adminCopy);
    }

    /**
     * Render a template with the given context
     */
    public String renderTemplate(String templateName, Context context) {
        // Escape the template name
        templateName = templateName
                .replace("..", "")
                .replace("/", "")
                .replace("\\", "");
        String templatePath = templateBase + templateName;
        return templateEngine.process(templatePath, context);
    }
}