package fr.utc.bpe_back.services;

import fr.utc.bpe_back.models.projects.Project;
import fr.utc.bpe_back.models.projects.ProjectStatus;
import fr.utc.bpe_back.models.projects.ProjectVote;
import fr.utc.bpe_back.models.sessions.Session;
import fr.utc.bpe_back.models.sessions.SessionTimelineStep;
import fr.utc.bpe_back.models.users.User;
import fr.utc.bpe_back.repositories.projects.ProjectRepository;
import fr.utc.bpe_back.repositories.projects.ProjectVoteRepository;
import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ProjectVoteService {
    private final Logger logger = LoggerFactory.getLogger(ProjectService.class);

    @Autowired
    ProjectVoteRepository projectVoteRepository;

    @Autowired
    SessionService sessionService;

    @Autowired
    ProjectRepository projectRepository;


    public Integer getVoteCountByProject(Project project) {
        return projectVoteRepository.getVoteCountByProject(project);
    }

    public void deleteByProject(Project project) {
        projectVoteRepository.deleteByProject(project);
    }

    @Scheduled(fixedRate = 3600 * 24 * 1000)
    @Transactional
    public void anonymizeVotesSchedule() {
        logger.info("Anonymizing votes for sessions that are over + 1 month");
        // For each session that is over + 1 month, anonymize the votes
        List<Session> sessions = sessionService.getSessionsWithSteps(true);

        for (Session session : sessions) {
            Date endDate = new Date(session.getEndDate().getTime());
            endDate.setTime(endDate.getTime() + 3600L * 24 * 1000 * 30);
            if (endDate.before(new Date())) {
                anonymizeVotesForSession(session);
            }
        }
    }

    @Transactional
    public void anonymizeVotesForSession(Session session) {
        logger.info("Anonymizing votes for session {}", session.getName());
        projectVoteRepository.anonymizeVotesForSession(session);
    }

    public Iterable<ProjectVote> getVotesForSessionAndUser(Session session, User user) {
        return projectVoteRepository.getVotesBySessionAndUser(session, user);
    }

    /**
     * Check if the session is open for voting
     * Check if the project is in accepted state
     * Vote for a project. If the user has already voted, ignore.
     * Check if the user is allowed to vote (utc-etu profile)
     * Check if the user has less than 3 votes for this session
     *
     * @param project the project to vote for
     * @param user the user who votes
     */
    public void voteForProject(Project project, User user) {
        Optional<SessionTimelineStep> step = sessionService.getSessionTimelineStepByName(project.getSession(), "VOTING");
        if (step.isEmpty()) {
            logger.warn("User {} tried to vote for project {} but the voting session step can't be found", user.getUsername(), project.getTitle());
            throw new IllegalStateException("Session voting step is not defined");
        }

        // Check if now is between the start and end date of the step
        if (step.get().getStart().after(new Date()) || step.get().getEnd().before(new Date())) {
            logger.warn("User {} tried to vote for project {} but the session is not open for voting", user.getUsername(), project.getTitle());
            throw new IllegalStateException("Session is not open for voting");
        }

        // Check if the project is in accepted state
        if (!Objects.equals(project.getStatus(), ProjectStatus.ACCEPTED)) {
            logger.warn("User {} tried to vote for project {} but the project is not in accepted state", user.getUsername(), project.getTitle());
            throw new IllegalStateException("Project is not in accepted state");
        }

        // Check if the user is a student
        if (!Objects.equals(user.getProfile(), "utc-etu") && !Objects.equals(user.getProfile(), "utc-etu3c")) {
            logger.warn("User {} is not allowed to vote", user.getUsername());
            throw new IllegalStateException("User is not allowed to vote");
        }

        // Check if the user has already voted
        Iterable<ProjectVote> votes = projectVoteRepository.getVotesBySessionAndUser(project.getSession(), user);
        for (ProjectVote vote : votes) {
            if (vote.getProject().getId().equals(project.getId())) {
                logger.warn("User {} has already voted for project {}", user.getUsername(), project.getTitle());
                throw new IllegalStateException("User has already voted for this project");
            }
        }

        // Check if the user has more than 3 votes for this session (votes)
        if ( votes.spliterator().getExactSizeIfKnown() >= 3 ) {
            logger.warn("User {} has already voted 3 times for session {}", user.getUsername(), project.getSession().getName());
            throw new IllegalStateException("User has already voted 3 times for this session");
        }

        ProjectVote vote = new ProjectVote();
        vote.setProject(project);
        vote.setUser(user);
        projectVoteRepository.save(vote);
    }

    /**
     * Delete a vote for a project
     * Check if the session is open for voting
     * Check if the project is in accepted state
     * @param project the project to vote for
     * @param user the user who votes
     */
    public void deleteVoteForProject(Project project, User user) {
        project = projectRepository.findById(project.getId()).orElseThrow();

        // Check if the session is open for voting
        Optional<SessionTimelineStep> step = sessionService.getSessionTimelineStepByName(project.getSession(), "VOTING");
        if (step.isEmpty()) {
            throw new IllegalStateException("Session voting step is not defined");
        }
        if (step.get().getStart().after(new Date()) || step.get().getEnd().before(new Date())) {
            throw new IllegalStateException("Session is not open for voting");
        }

        // Check if the project is in accepted state
        if (!Objects.equals(project.getStatus(), ProjectStatus.ACCEPTED)) {
            throw new IllegalStateException("Project is not in accepted state");
        }

        // Retrieve the vote and delete it
        Iterable<ProjectVote> votes = projectVoteRepository.getVoteByProjectAndUser(project, user);
        for (ProjectVote vote : votes) {
            if (vote.getUser().getId().equals(user.getId())) {
                projectVoteRepository.delete(vote);
                return;
            }
        }

    }
}