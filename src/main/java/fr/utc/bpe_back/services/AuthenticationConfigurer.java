package fr.utc.bpe_back.services;

import fr.utc.bpe_back.models.users.User;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

public interface AuthenticationConfigurer {
	public User mapUser(String username);
	public void configure(AuthenticationManagerBuilder auth) throws Exception;
}
