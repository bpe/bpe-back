package fr.utc.bpe_back.services;

import fr.utc.bpe_back.models.sessions.Session;
import fr.utc.bpe_back.models.sessions.SessionTimelineStep;
import fr.utc.bpe_back.models.sessions.TimelineItem;
import fr.utc.bpe_back.repositories.sessions.SessionRepository;
import fr.utc.bpe_back.repositories.sessions.SessionTimelineStepRepository;
import fr.utc.bpe_back.repositories.sessions.TimelineItemRepository;
import jakarta.persistence.EntityExistsException;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Data
@Service
@Transactional
public class SessionService {
    @Autowired
    private SessionRepository sessionRepository;

    @Autowired
    TimelineItemRepository timelineItemRepository;

    @Autowired
    SessionTimelineStepRepository sessionTimelineStepRepository;

    public Optional<Session> getSession(final Long id) {
        return sessionRepository.findById(id);
    }

    public Iterable<Session> getSessions(boolean includeDeleted) {
        if (includeDeleted) {
            return sessionRepository.findAll();
        } else {
            return sessionRepository.findAllByDeletedIsFalse();
        }
    }

    public void deleteSession(final Long id) {
        sessionRepository.deleteById(id);
    }

    public Session saveSession(Session session) {
        return sessionRepository.save(session);
    }

    public Iterable<TimelineItem> getTimelineItems() {
        return timelineItemRepository.findAll();
    }

    public TimelineItem createTimelineItem(final TimelineItem ti) {
        if (timelineItemRepository.findById(ti.getId()).isPresent()) {
            throw new IllegalArgumentException("ID already in use");
        }
        return timelineItemRepository.save(ti);
    }

    public Optional<TimelineItem> getTimelineItem(final String id) {
        return timelineItemRepository.findById(id);
    }

    public Session getSessionBySlug(final String slug) {
        return sessionRepository.getSessionBySlug(slug);
    }

    public SessionTimelineStep createTimelineStep(SessionTimelineStep sessionTimelineStep) {
        // Check if a same type timelineItem exists for session
        if (sessionTimelineStepRepository.getTimelineStepByItemForSession(
                sessionTimelineStep.getSession(), sessionTimelineStep.getTimelineItem()
        ).isPresent()) {
            throw new EntityExistsException("A step of the same type already exists for this session");
        }
        return sessionTimelineStepRepository.save(sessionTimelineStep);
    }

    public List<Session> getSessionsWithSteps(boolean withDeleted) {
        Iterable<Session> sessions = getSessions(withDeleted);
        List<Session> sessionsWithComments = new ArrayList<>();
        sessions.forEach(session -> {
            session.setTimelineSteps(sessionTimelineStepRepository.getTimelineStepsForSession(session));
            sessionsWithComments.add(session);
        });

        return sessionsWithComments;
    }

    public void deleteSessionTimelineStepsByName(Session session, TimelineItem timelineItem) {
        sessionTimelineStepRepository.deleteBySessionAndName(session, timelineItem);
    }

    public Optional<Session> getSessionWithSteps(Long id) {
        Optional<Session> session = getSession(id);
        session.ifPresent(value -> value.setTimelineSteps(sessionTimelineStepRepository.getTimelineStepsForSession(value)));
        return session;
    }

    public Optional<SessionTimelineStep> getSessionTimelineStep(Long id) {
        return sessionTimelineStepRepository.findById(id);
    }

    public Optional<SessionTimelineStep> getSessionTimelineStepByName(Session s, String name) {
        return sessionTimelineStepRepository.getTimelineStepByNameForSession(s, name);
    }

    public SessionTimelineStep saveSessionTimelineStep(SessionTimelineStep sessionTimelineStep) {
        return sessionTimelineStepRepository.save(sessionTimelineStep);
    }

    public Optional<SessionTimelineStep> getCurrentSessionTimelineStep(Session session) {
        Set<SessionTimelineStep> steps = sessionTimelineStepRepository.getActiveSessionTimelineStep(session);
        if (steps.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(steps.iterator().next());
    }
}
