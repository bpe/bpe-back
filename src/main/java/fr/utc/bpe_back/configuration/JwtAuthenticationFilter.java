package fr.utc.bpe_back.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.utc.bpe_back.services.JwtTokenService;
import io.jsonwebtoken.JwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import java.io.IOException;
import java.util.Optional;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
	private static final String BEARER = "Bearer";

	@Autowired
	private JwtTokenService tokenService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull FilterChain chain)
			throws IOException, ServletException
	{
		// Assume we have only one Authorization header value
		final Optional<String> token = Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION));

		if (SecurityContextHolder.getContext().getAuthentication() == null && 
				token.isPresent() && token.get().startsWith(BEARER))
		{
			String bearerToken = token.get().substring(BEARER.length()+1);

			try {
				tokenService.authenticate(bearerToken);
			} catch(JwtException e) {
				String message = e.getMessage();
				logger.warn(message);

				response.setStatus(HttpStatus.UNAUTHORIZED.value());
				response.setContentType(MediaType.APPLICATION_JSON_VALUE);

				ObjectMapper mapper = new ObjectMapper();
				mapper.writeValue(response.getWriter(), e.getMessage());
				return;
			}
		}

		chain.doFilter(request, response);
		SecurityContextHolder.clearContext();
	}
}
