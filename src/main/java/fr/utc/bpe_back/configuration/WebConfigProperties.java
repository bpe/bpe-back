package fr.utc.bpe_back.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "web")
@Component
public class WebConfigProperties {

    private Cors cors;

    public Cors getCors() {
        return cors;
    }

    public void setCors(Cors cors) {
        this.cors = cors;
    }

    public record Cors(String[] allowedOrigins, String[] allowedMethods, long maxAge, String[] allowedHeaders,
                       String[] exposedHeaders) {
    }
}