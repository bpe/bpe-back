package fr.utc.bpe_back.CAS;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Map;


public class CasValidationResponse {
    @JsonProperty("serviceResponse")
    private CasServiceResponse serviceResponse;

    public CasServiceResponse.AuthenticationSuccess getProfile() {
        return serviceResponse.authenticationSuccess;
    }

    public boolean authenticationFailed() {
        return serviceResponse.authenticationSuccess == null;
    }

    public String getFailureCode() {
        if (serviceResponse != null && serviceResponse.authenticationFailure != null) {
            return serviceResponse.authenticationFailure.code;
        }
        else {
            return "Unknown code";
        }
    }



    public static class CasServiceResponse {
        @JsonProperty("authenticationSuccess")
        private AuthenticationSuccess authenticationSuccess;

        @JsonProperty("authenticationFailure")
        private AuthenticationFailure authenticationFailure;

        private static class AuthenticationFailure {
            @JsonProperty("code")
            public String code;

            @JsonProperty("description")
            public String description;
        }

        public static class AuthenticationSuccess {

            @JsonProperty("user")
            private String username;

            @JsonProperty("attributes")
            private Map<String, List<String>> attributes;

            public String getEmail() {
                return attributes.getOrDefault("mail", null).get(0);
            }

            public String getAccountProfile() {
                return attributes.getOrDefault("accountProfile", null).get(0);
            }

            public String getName() {
                return attributes.getOrDefault("displayName", null).get(0);
            }

            public String getUsername() {
                return username;
            }
        }
    }
}