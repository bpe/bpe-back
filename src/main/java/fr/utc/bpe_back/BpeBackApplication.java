package fr.utc.bpe_back;

import fr.utc.bpe_back.configuration.WebConfigProperties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(WebConfigProperties.class)
public class BpeBackApplication {
    public static void main(String[] args) {
        SpringApplication.run(BpeBackApplication.class, args);
    }
}
