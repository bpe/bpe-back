# BPE Back

API du budget participatif étudiant de l'UTC. 

## Prérequis
- Java
- Maven
- PostgreSQL

## Installation
C'est un projet Spring Boot, donc il suffit de lancer la classe `BpeBackApplication` pour démarrer le serveur.

## Configuration
Le fichier `application.properties.example` contient la configuration de l'application. 
Il faut le renommer en `application.properties` et modifier les valeurs pour correspondre à votre environnement.